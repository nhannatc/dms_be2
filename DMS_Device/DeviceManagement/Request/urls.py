from django.urls import path
from . import views

urlpatterns = [
    path('', views.ListCreateDeviceView.as_view(), name="List Request"),
    path('search', views.ListSearchallView.as_view(), name="Search"),
    path('search/notwait', views.ListSearchallnotWaitView.as_view(), name="Search"),
    path('user/', views.ListAllRequestWaitofUserView.as_view(), name="List Request Wait"),
    path('user/tracking/', views.ListAllRequestofUserView.as_view(), name="List Request not Wait"),
    path('accept/<int:pk>/', views.UpdateRequestAcceptView.as_view(), name='Device Tracking'),
    path('reject/<int:pk>/', views.UpdateRequestRejectView.as_view(), name='Device Tracking'),
    path('reject/user/<int:pk>/', views.UpdateRequestRejectByUserView.as_view(), name='Device Tracking'),
    path('return/<int:pk>/', views.UpdateRequestReturnView.as_view(), name='Device Tracking'),
    path('return/admin/<int:pk>/', views.UpdateRequestReturnbyAdminView.as_view(), name='Device Tracking'),
    path('all/', views.ListAllRequestnotWaitView.as_view(), name='Device Tracking'),
    path('accept/', views.ListDeviceAcceptView.as_view(), name='List Accept'),
    path('reject/', views.ListDeviceRejectView.as_view(), name='List Reject'),
    path('return/', views.ListDeviceReturnView.as_view(), name='List Reject'),
    path('status/', views.ListCreateStatusView.as_view(), name='Status'),
    path('status/<int:pk>/', views.UpdateDeleteStatusView.as_view(), name='Status'),
]
