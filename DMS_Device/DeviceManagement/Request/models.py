from django.db import models
from django.utils.translation import ugettext_lazy as _

from Device.models import DeviceProfile
from User.models import UserProfile

# Create your models here.
class Statues(models.Model):
    name = models.CharField(max_length=254, verbose_name=_('name'))

    def __str__(self):
        return self.name

class RequestProfile(models.Model):
    devices_id = models.ForeignKey(DeviceProfile, verbose_name=_('Device'),
                                   on_delete=models.CASCADE)
    user_id = models.ForeignKey(UserProfile, verbose_name=_('User'),
                                   on_delete=models.CASCADE)
    statues_id = models.ForeignKey(Statues, null=True, verbose_name=_('status'),
                                   on_delete=models.CASCADE, default=1)
    createdDate = models.DateTimeField(null=False, verbose_name=_('Created Date'), auto_now_add=True)
    updatedDate = models.DateTimeField(verbose_name=_('Update Date'), null=True)
    deletedDate = models.DateTimeField(verbose_name=_('Delete Date'), null=True)
    startDate = models.DateTimeField(verbose_name=_('Start Date'), null=True)
    endDate = models.DateTimeField(verbose_name=_('End Date'), null=True)

    def __str__(self):
        return self.status

    class Meta:
        verbose_name = _('Devices')
        verbose_name_plural = _('Devices')
        ordering = ['-createdDate']
        get_latest_by = 'endDate'