from rest_framework import serializers
from .models import RequestProfile, Statues

from Device.serializer import DeviceSerializerforRequest
from User.serializer import UserSerializerforRequest

class ListRequestSerializer(serializers.ModelSerializer):
    devices_name = serializers.CharField(source='devices_id', read_only=True)
    user_name = serializers.CharField(source='user_id', read_only=True)
    statues_name = serializers.CharField(source='statues_id', read_only=True)

    class Meta:
        model = RequestProfile
        fields = ('id', 'devices_id', 'devices_name', 'user_id', 'user_name', 'statues_id', 'statues_name', 'createdDate', 'updatedDate', 'deletedDate', 'startDate', 'endDate')
        extra_kwargs = {'id': {'read_only': True}, 'statues_id': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}, 'startDate': {'read_only': True}, 'endDate': {'read_only': True}}

class ListRequetstDeviceSerializer(serializers.ModelSerializer):
   devices_id = DeviceSerializerforRequest()
   user_id = UserSerializerforRequest()
   statues_name = serializers.CharField(source='statues_id', read_only=True)
   class Meta:
       model = RequestProfile
       fields = (
       'id', 'devices_id', 'user_id', 'statues_id', 'statues_name', 'createdDate',
       'updatedDate', 'deletedDate', 'startDate', 'endDate')
       extra_kwargs = {'id': {'read_only': True}, 'statues_id': {'read_only': True}, 'createdDate': {'read_only': True},
                       'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True},
                       'startDate': {'read_only': True}, 'endDate': {'read_only': True}}


class RequestSerializer(serializers.ModelSerializer):
    devices_name = serializers.CharField(source='devices_id', read_only=True)
    user_name = serializers.CharField(source='user_id', read_only=True)
    statues_name = serializers.CharField(source='statues_id', read_only=True)
    class Meta:
        model = RequestProfile
        fields = ('id', 'devices_id', 'devices_name', 'user_id', 'user_name', 'statues_id', 'statues_name', 'createdDate', 'updatedDate', 'deletedDate', 'startDate', 'endDate')
        extra_kwargs = {'id': {'read_only': True}, 'statues_id': {'read_only': True}, 'devices_id': {'read_only': True}, 'user_id': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}, 'startDate': {'read_only': True}, 'endDate': {'read_only': True}}

class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statues
        fields = '__all__'
        extra_kwargs = {'id': {'read_only': True},}

