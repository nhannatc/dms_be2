
from django.db.models import Q

import datetime

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django_filters import filters, FilterSet
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework.permissions import IsAuthenticated
from rest_framework import status, generics
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveUpdateAPIView, ListAPIView

from .models import RequestProfile, Statues
from .serializer import ListRequestSerializer, RequestSerializer, StatusSerializer, ListRequetstDeviceSerializer

from Device.models import DeviceProfile
from User.models import UserProfile

# Create your views here.



class ListCreateDeviceView(ListCreateAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RequestProfile.objects.all().filter(statues_id__exact=1).order_by('-id')

    def create(self, request, *args, **kwargs):
        deletedDate = self.request.user.deletedDate
        serializer = ListRequestSerializer(data=request.data)
        if serializer.is_valid() and deletedDate == None:
            serializer.save()
            return JsonResponse({
                'message': 'Create a new Request successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new Request unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)
class UpdateRequestAcceptView(RetrieveUpdateAPIView):
    model = RequestProfile
    serializer_class = RequestSerializer
    queryset = RequestProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        requests = get_object_or_404(RequestProfile, id=kwargs.get('pk'))
        deletedDate = self.request.user.deletedDate
        deviceprofile = get_object_or_404(DeviceProfile, id=requests.devices_id_id)
        serializer = RequestSerializer(requests, data=request.data)
        if serializer.is_valid() and role == 1 and deletedDate == None and requests.statues_id.name == 'Wait' and deviceprofile.statues_id == 2:
            serializer.validated_data['statues_id_id'] = 2
            serializer.validated_data['startDate'] = datetime.datetime.now()
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.save()


            deviceprofile.statues_id = 1
            deviceprofile.user_id_id = requests.user_id_id
            deviceprofile.updatedDate = datetime.datetime.now()
            deviceprofile.save()
            return JsonResponse({
                            'message': 'Accept request successful!'
                        }, status=status.HTTP_200_OK)

        return JsonResponse({
                      'message': 'set Accept unsuccessful!'
                    }, status=status.HTTP_400_BAD_REQUEST)


class UpdateRequestRejectView(RetrieveUpdateAPIView):
        model = RequestProfile
        serializer_class = RequestSerializer
        queryset = RequestProfile.objects.all()

        permission_classes = (IsAuthenticated,)
        lookup_field = "pk"

        def put(self, request, *args, **kwargs):
            role = self.request.user.role_id
            deletedDate = self.request.user.deletedDate
            requests = get_object_or_404(RequestProfile, id=kwargs.get('pk'))
            serializer = RequestSerializer(requests, data=request.data)
            if serializer.is_valid() and role == 1 and deletedDate == None and requests.statues_id.name == 'Wait':
                serializer.validated_data['statues_id_id'] = 3
                serializer.validated_data['updatedDate'] = datetime.datetime.now()
                serializer.validated_data['deletedDate'] = datetime.datetime.now()
                serializer.save()
                return JsonResponse({
                    'message': 'Reject request successful!'
                }, status=status.HTTP_200_OK)
                return JsonResponse({
                    'message': 'set reject unsuccessful!'
                }, status=status.HTTP_400_BAD_REQUEST)


class UpdateRequestRejectByUserView(RetrieveUpdateAPIView):
    model = RequestProfile
    serializer_class = RequestSerializer
    queryset = RequestProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def put(self, request, *args, **kwargs):
        user_id = self.request.user.id
        deletedDate = self.request.user.deletedDate
        requests = get_object_or_404(RequestProfile, id=kwargs.get('pk'))
        serializer = RequestSerializer(requests, data=request.data)
        if serializer.is_valid() and requests.user_id_id == user_id and deletedDate == None and requests.statues_id.name == 'Wait':
            serializer.validated_data['statues_id_id'] = 3
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.validated_data['deletedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                'message': 'Reject request successful!'
            }, status=status.HTTP_200_OK)
            return JsonResponse({
               # 'id': requests.get.statues_id,
                'message': 'set reject unsuccessful!'
            }, status=status.HTTP_400_BAD_REQUEST)


class UpdateRequestReturnView(RetrieveUpdateAPIView):
    model = RequestProfile
    serializer_class = RequestSerializer
    queryset = RequestProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def put(self, request, *args, **kwargs):
        user_id = self.request.user.id
        deletedDate = self.request.user.deletedDate
        requests = get_object_or_404(RequestProfile, id=kwargs.get('pk'))
        deviceprofile = get_object_or_404(DeviceProfile, id=requests.devices_id_id)
        serializer = RequestSerializer(requests, data=request.data)
        if serializer.is_valid() and requests.user_id_id == user_id and deletedDate == None and requests.statues_id.name == 'Accept' and deviceprofile.statues_id == 1:
            serializer.validated_data['statues_id_id'] = 4
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.validated_data['endDate'] = datetime.datetime.now()
            serializer.save()


            deviceprofile.statues_id = 2
            deviceprofile.user_id_id = None
            deviceprofile.updatedDate = datetime.datetime.now()
            deviceprofile.save()
            return JsonResponse({
                'message': 'Return request successful!'
            }, status=status.HTTP_200_OK)
        return JsonResponse({
                'message': 'set Return unsuccessful!'
            }, status=status.HTTP_400_BAD_REQUEST)


class UpdateRequestReturnbyAdminView(RetrieveUpdateAPIView):
    model = RequestProfile
    serializer_class = RequestSerializer
    queryset = RequestProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        requests = get_object_or_404(RequestProfile, id=kwargs.get('pk'))
        deletedDate = self.request.user.deletedDate
        deviceprofile = get_object_or_404(DeviceProfile, id=requests.devices_id_id)
        serializer = RequestSerializer(requests, data=request.data)
        if serializer.is_valid() and role == 1 and deletedDate == None and requests.statues_id.name == 'Accept' and deviceprofile.statues_id == 1:
            serializer.validated_data['statues_id_id'] = 4
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.validated_data['endDate'] = datetime.datetime.now()
            serializer.save()


            deviceprofile.statues_id = 2
            deviceprofile.user_id_id = None
            deviceprofile.updatedDate = datetime.datetime.now()
            deviceprofile.save()
            return JsonResponse({
                'message': 'Return request successful!'
            }, status=status.HTTP_200_OK)
        return JsonResponse({
            'message': 'set Return unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)


class ListDeviceAcceptView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RequestProfile.objects.all().filter(deletedDate__isnull=True, endDate__isnull=True, statues_id__exact=2).order_by('-id')


class ListDeviceRejectView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RequestProfile.objects.all().filter(deletedDate__isnull=False, endDate__isnull=True, statues_id__exact=3).order_by('-id')


class ListDeviceReturnView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RequestProfile.objects.all().filter(deletedDate__isnull=True, endDate__isnull=False, statues_id__exact=4).order_by('-id')

class ListAllRequestnotWaitView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RequestProfile.objects.all().filter(~Q(statues_id=1)).order_by('-id')

class ListAllRequestofUserView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user_id=self.request.user.id
        return RequestProfile.objects.all().filter(~Q(statues_id=1), user_id__exact=user_id).order_by('-id')

class ListAllRequestWaitofUserView(ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user_id=self.request.user.id
        return RequestProfile.objects.all().filter(statues_id__exact=1, user_id__exact=user_id).order_by('-id')

class RequestFilter(FilterSet):
    devicename = filters.CharFilter(field_name="devices_id__name", lookup_expr='contains')
    email = filters.CharFilter(field_name="user_id__email", lookup_expr='contains')
    displayname = filters.CharFilter(field_name="user_id__displayName", lookup_expr='contains')
    status = filters.CharFilter(field_name="statues_id__name", lookup_expr='exact')
    category = filters.CharFilter(field_name="devices_id__categories__name", lookup_expr='exact')
    startDate = filters.DateTimeFilter(field_name="createdDate", lookup_expr="gte")
    endDate = filters.DateTimeFilter(field_name="updatedDate", lookup_expr="lte")
    user_id = filters.NumberFilter(field_name="user_id__id", lookup_expr='exact')

    class Meta:
        model = RequestProfile
        fields = '__all__'

class ListSearchallView(generics.ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = RequestProfile.objects.all().order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filterset_class = RequestFilter

class ListSearchallnotWaitView(generics.ListAPIView):
    model = RequestProfile
    serializer_class = ListRequetstDeviceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = RequestProfile.objects.all().filter(~Q(statues_id=1)).order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filterset_class = RequestFilter

class ListCreateStatusView(ListCreateAPIView):
    model = Statues
    serializer_class = StatusSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Statues.objects.all().order_by('-id')

    def create(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        serializer = StatusSerializer(data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Create a new Status successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new Status unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)


class UpdateDeleteStatusView(RetrieveUpdateDestroyAPIView):
    model = Statues
    serializer_class = StatusSerializer
    queryset = Statues.objects.all()
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        statues = get_object_or_404(Statues, id=kwargs.get('pk'))
        serializer = StatusSerializer(statues, data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Update Status successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Update Status unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        statues = get_object_or_404(DeviceProfile, id=kwargs.get('pk'))
        role = self.request.user.role_id
        if role == 1:
            statues.delete()

            return JsonResponse({
                'message': 'Delete Status successful!'
            }, status=status.HTTP_200_OK)
