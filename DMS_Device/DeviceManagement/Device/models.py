from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import date
from django.utils.text import slugify

from User.models import UserProfile
# Create your models here.

class Categories(models.Model):
    name = models.CharField(max_length=254, verbose_name=_('name'))

    def __str__(self):
        return self.name

class Statues(models.Model):
    name = models.CharField(max_length=254, verbose_name=_('name'))

    def __str__(self):
        return self.name

class DeviceProfile(models.Model):
    user_id = models.ForeignKey(UserProfile, null=True, verbose_name=_('User'),
                                   on_delete=models.CASCADE)
    categories = models.ForeignKey(Categories, null=True, verbose_name=_('Category'),
                                   on_delete=models.CASCADE)
    statues = models.ForeignKey(Statues, null=True, verbose_name=_('Status'),
                                   on_delete=models.CASCADE)
    name = models.CharField(max_length=254, verbose_name=_('name'))
    slug = models.SlugField(max_length=254, unique=True, blank=True, editable=True)
    description = models.TextField(verbose_name=_('Description'))
    createdDate = models.DateTimeField(null=False, verbose_name=_('Created Date'), auto_now_add=True)
    updatedDate = models.DateTimeField(verbose_name=_('Update Date'), null=True)
    deletedDate = models.DateTimeField(verbose_name=_('Delete Date'), null=True)
    upload_to = 'img/device/{0}/{1}'.format(date.today().year, date.today().month)
    feature_img = models.ImageField(upload_to=upload_to, blank=True, null=True, max_length=254, verbose_name=_('Feature Image'))

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(DeviceProfile, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Devices')
        verbose_name_plural = _('Devices')
        ordering = ['-updatedDate']
        get_latest_by = 'createdDate'