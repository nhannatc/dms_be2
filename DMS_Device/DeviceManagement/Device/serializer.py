from rest_framework import serializers

from .models import DeviceProfile, Statues, Categories
from User.serializer import UserSerializerforRequest


class ListDeviceSerializer(serializers.ModelSerializer):
    categories_name = serializers.CharField(source='categories', read_only=True)
    statues_name = serializers.CharField(source='statues', read_only=True)
    user_id = UserSerializerforRequest()
    class Meta:
        model = DeviceProfile
        fields = ('id', 'feature_img', 'name', 'categories', 'categories_name', 'description', 'statues', 'statues_name', 'user_id', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'id': {'read_only': True}, 'user_id': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class DeviceSerializer(serializers.ModelSerializer):
    categories_name = serializers.CharField(source='categories', read_only=True)
    statues_name = serializers.CharField(source='statues', read_only=True)
    user_name = serializers.CharField(source='user_id', read_only=True)
    class Meta:
        model = DeviceProfile
        fields = ('id', 'feature_img', 'name', 'categories', 'categories_name', 'description', 'statues', 'statues_name', 'user_id', 'user_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'id': {'read_only': True}, 'user_id': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class DeviceSerializerforRequest(serializers.ModelSerializer):
    categories_name = serializers.CharField(source='categories', read_only=True)
    class Meta:
        model = DeviceProfile
        fields = ('id', 'name', 'categories', 'categories_name', 'description')
        extra_kwargs = {'id': {'read_only': True}, 'name': {'read_only': True}, 'categories': {'read_only': True}, 'description': {'read_only': True}}

class DeviceUpdateRunorWaitSerializer(serializers.ModelSerializer):
    categories_name = serializers.CharField(source='categories', read_only=True)
    statues_name = serializers.CharField(source='statues', read_only=True)
    class Meta:
        model = DeviceProfile
        fields = ('id', 'feature_img', 'name', 'categories', 'categories_name', 'description', 'statues', 'statues_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'id': {'read_only': True}, 'feature_img': {'read_only': True}, 'name': {'read_only': True}, 'categories': {'read_only': True}, 'description': {'read_only': True}, 'statues': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}


class DeviceSerializerDelete(serializers.ModelSerializer):
    categories_name = serializers.CharField(source='categories', read_only=True)
    statues_name = serializers.CharField(source='statues', read_only=True)
    user_name = serializers.CharField(source='user_id', read_only=True)
    class Meta:
        model = DeviceProfile
        fields = ('id', 'feature_img', 'name', 'categories', 'categories_name', 'description', 'statues', 'statues_name', 'user_id', 'user_name',
        'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'id': {'read_only': True}, 'user_id': {'read_only': True}, 'feature_img': {'read_only': True}, 'name': {'read_only': True}, 'categories': {'read_only': True}, 'description': {'read_only': True}, 'statues': {'read_only': True}, 'createdDate': {'read_only': True},
                        'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statues
        fields = '__all__'
        extra_kwargs = {'id': {'read_only': True},}

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = '__all__'
        extra_kwargs = {'id': {'read_only': True},}