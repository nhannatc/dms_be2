import datetime
from django_filters import filters, FilterSet

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework.permissions import IsAuthenticated
from rest_framework import status, generics
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveUpdateAPIView, ListAPIView


from .models import DeviceProfile, Statues, Categories
from .serializer import DeviceSerializer, StatusSerializer, CategorySerializer, DeviceSerializerDelete, ListDeviceSerializer


# Create your views here.
class ListCreateDeviceView(ListCreateAPIView):
    model = DeviceProfile
    serializer_class = ListDeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return DeviceProfile.objects.all().filter(deletedDate__isnull=True).order_by('-id')

    def create(self, request, *args, **kwargs):
        role = self.request.user.role_id
        serializer = DeviceSerializer(data=request.data)
        deletedDate = self.request.user.deletedDate

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Create a new Device successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new Device unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class ListWaitDeviceView(ListAPIView):
    model = DeviceProfile
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return DeviceProfile.objects.all().filter(deletedDate__isnull=True, statues__exact=2).order_by('-id')

class ListRunDeviceView(ListAPIView):
    model = DeviceProfile
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return DeviceProfile.objects.all().filter(deletedDate__isnull=True, statues__exact=1).order_by('-id')

class ListDeleteDeviceView(ListAPIView):
    model = DeviceProfile
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        role = self.request.user.role_id
        deleted = self.request.user.deletedDate
        if role == 1 and deleted == None:# 1 là admin
            return DeviceProfile.objects.all().filter(deletedDate__isnull=False).order_by('-id')

class ListCreateStatusView(ListCreateAPIView):
    model = Statues
    serializer_class = StatusSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Statues.objects.all().order_by('-id')

    def create(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        serializer = StatusSerializer(data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Create a new Status successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new Status unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class ListCreateCategoryView(ListCreateAPIView):
    model = Categories
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Categories.objects.all().order_by('-id')

    def create(self, request, *args, **kwargs):
        role = self.request.user.role_id
        serializer = CategorySerializer(data=request.data)
        deletedDate = self.request.user.deletedDate

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Create a new Category successful!'
            }, status=status.HTTP_201_CREATED)

        return JsonResponse({
            'message': 'Create a new Category unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class UpdateDeleteDeviceView(RetrieveUpdateAPIView):
    model = DeviceProfile
    serializer_class = DeviceSerializer
    queryset = DeviceProfile.objects.all()
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        device = get_object_or_404(DeviceProfile, id=kwargs.get('pk'))
        serializer = DeviceSerializer(device, data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                'message': 'Update Device successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'data': serializer.data,
            'message': 'Update Device unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class DeleteDeviceView(RetrieveUpdateAPIView):
        model = DeviceProfile
        serializer_class = DeviceSerializerDelete
        queryset = DeviceProfile.objects.all()
        permission_classes = (IsAuthenticated,)

        def put(self, request, *args, **kwargs):
            role = self.request.user.role_id
            deletedDate = self.request.user.deletedDate
            device = get_object_or_404(DeviceProfile, id=kwargs.get('pk'))
            serializer = DeviceSerializerDelete(device, data=request.data)

            if serializer.is_valid() and role == 1 and deletedDate == None:
                serializer.validated_data['deletedDate'] = datetime.datetime.now()
                serializer.save()

                return JsonResponse({
                    'message': 'Delete Device successful!'
                }, status=status.HTTP_200_OK)

            return JsonResponse({
                'message': 'Delete Device unsuccessful!'
            }, status=status.HTTP_400_BAD_REQUEST)

class UpdateDeleteStatusView(RetrieveUpdateDestroyAPIView):
    model = Statues
    serializer_class = StatusSerializer
    queryset = Statues.objects.all()
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        statues = get_object_or_404(Statues, id=kwargs.get('pk'))
        serializer = StatusSerializer(statues, data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Update Status successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Update Status unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        statues = get_object_or_404(DeviceProfile, id=kwargs.get('pk'))
        role = self.request.user.role_id
        if role == 1:
            statues.delete()

            return JsonResponse({
                'message': 'Delete Status successful!'
            }, status=status.HTTP_200_OK)

class UpdateDeleteCategoryView(RetrieveUpdateDestroyAPIView):
    model = Categories
    serializer_class = CategorySerializer
    queryset = Categories.objects.all()
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        category = get_object_or_404(Categories, id=kwargs.get('pk'))
        serializer = CategorySerializer(category, data=request.data)

        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.save()

            return JsonResponse({
                'message': 'Update Category successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Update Category unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        category = get_object_or_404(DeviceProfile, id=kwargs.get('pk'))
        role = self.request.user.role_id
        if role == 1:
            category.delete()

            return JsonResponse({
                'message': 'Delete Category successful!'
            }, status=status.HTTP_200_OK)


class DeviceFilter(FilterSet):
    name = filters.CharFilter(field_name="name", lookup_expr='contains')
    email = filters.CharFilter(field_name="user_id__email", lookup_expr='contains')
    displayname = filters.CharFilter(field_name="user_id__displayName", lookup_expr='contains')
    status = filters.CharFilter(field_name="statues__name", lookup_expr='exact')
    category = filters.CharFilter(field_name="categories__name", lookup_expr='exact')

    class Meta:
        model = DeviceProfile
        fields = ('name', 'user_id__email', 'user_id__displayName', 'statues__name', 'categories__name')


class ListSearchallView(generics.ListAPIView):
    model = DeviceProfile
    serializer_class = ListDeviceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = DeviceProfile.objects.all().filter(deletedDate__isnull=True).order_by('-id')
    filter_backends = [DjangoFilterBackend]
    filterset_class = DeviceFilter