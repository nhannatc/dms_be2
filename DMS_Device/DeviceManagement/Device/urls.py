from django.urls import path
from . import views
from .views import ListCreateDeviceView, UpdateDeleteStatusView, UpdateDeleteCategoryView, UpdateDeleteDeviceView, ListCreateStatusView , ListCreateCategoryView

urlpatterns = [
    path('', ListCreateDeviceView.as_view(), name='Devices'),
    path('search', views.ListSearchallView.as_view(), name='Search Devices'),
    path('wait/', views.ListWaitDeviceView.as_view(), name='Wait Devices'),
    path('run/', views.ListRunDeviceView.as_view(), name='Run Devices'),
    path('deleted/', views.ListDeleteDeviceView.as_view(), name='Devices'),
    path('category/', ListCreateCategoryView.as_view(), name='Category'),
    path('category/<int:pk>/', UpdateDeleteCategoryView.as_view(), name='Category'),
    path('status/', ListCreateStatusView.as_view(), name='Status'),
    path('status/<int:pk>/', UpdateDeleteStatusView.as_view(), name='Status'),
    path('<int:pk>/', UpdateDeleteDeviceView.as_view(), name='Update Devices'),
    path('<int:pk>/delete/', views.DeleteDeviceView.as_view(), name='Delete Devices'),
]