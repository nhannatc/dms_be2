from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from datetime import date

# Create your models here.

class UserType(models.Model):
    name = models.CharField(max_length=254, verbose_name=_('name'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Role')
        verbose_name_plural = _('UserType')
class UserProfile(AbstractUser):
    # Delete not use field
    username = None
    last_login = None
    is_staff = None
    is_superuser = None

    role = models.ForeignKey(UserType, on_delete=models.CASCADE, verbose_name=_('Role'))
    email = models.EmailField(verbose_name='email address', unique=True, max_length=244)
    password = models.TextField(null=False)
    displayName = models.CharField(null=False, max_length=255)
    upload_to = 'img/user/{0}/{1}'.format(date.today().year, date.today().month)
    image = models.ImageField(upload_to=upload_to, blank=True, null=True, max_length=254, verbose_name=_('Feature Image'))
    createdDate = models.DateTimeField(null=False, verbose_name=_('Created Date'), auto_now_add=True)
    updatedDate = models.DateTimeField(null=True)
    deletedDate = models.DateTimeField(null=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email
