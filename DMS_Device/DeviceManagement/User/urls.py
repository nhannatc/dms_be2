from django.urls import path
from . import views
from rest_framework_simplejwt import views as jwt_views
from .views import UserLoginView, LogoutView, ListCreateUserView
urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('login/refresh/', jwt_views.TokenRefreshView().as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('', ListCreateUserView.as_view(), name='Users'), # xem danh sách User
    path('deleted/', views.ListDeleteUserView.as_view(), name='Deleted Users'),
    path('user/', views.UpdateUserView.as_view(), name='Update'), # xem, sửa, xóa thông tin User
    path('<int:pk>/', views.UpdateUserFormAdminView.as_view(), name='Update'), # xem, sửa, xóa thông tin User
    path('user/password/', views.UpdatePasswordUserView.as_view(), name='Change Password'),
    path('user/delete/', views.DeleteUserView.as_view(), name='Delete'),
    path('<int:pk>/delete/', views.DeleteUserFormAdminView.as_view(), name='Delete'),
    path('search', views.ListSearchallView.as_view(), name='Users'), # xem danh sách User

]



