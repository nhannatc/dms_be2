from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken, TokenError


from .models import UserProfile

class ListUserSerializer(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role', read_only=True)

    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'password', 'displayName', 'image', 'role', 'role_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'password': {'write_only': True}, 'id': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class UserSerializer(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role', read_only=True)
    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'displayName', 'password', 'image', 'role', 'role_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'role': {'read_only': True}, 'password': {'read_only': True}, 'id': {'read_only': True}, 'email': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class UserByAdminSerializer(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role', read_only=True)
    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'displayName', 'password', 'image', 'role', 'role_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'password': {'read_only': True}, 'id': {'read_only': True}, 'email': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class UserSerializerforRequest(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role', read_only=True)
    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'displayName', 'role', 'role_name')
        extra_kwargs = {'role': {'read_only': True}, 'role_name': {'read_only': True}, 'id': {'read_only': True}, 'email': {'read_only': True}, 'displayName': {'read_only': True}}


class UserSerializerDelete(serializers.ModelSerializer):
    role_name = serializers.CharField(source='role', read_only=True)
    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'displayName', 'password', 'image', 'role_name', 'createdDate', 'updatedDate', 'deletedDate')
        extra_kwargs = {'id': {'read_only': True}, 'email': {'read_only': True}, 'displayName': {'read_only': True}, 'image': {'read_only': True}, 'password': {'read_only': True}, 'role': {'read_only': True}, 'createdDate': {'read_only': True}, 'updatedDate': {'read_only': True}, 'deletedDate': {'read_only': True}}

class UserPasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('password',)
        extra_kwargs = {'password': {'write_only': True}}


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True, write_only=True)
    #deletedDate = serializers.DateTimeField(read_only=True)

class UserLogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_messages = {
        'error_token': ('Token is expired or invalid')
    }

    def validate(self, attr):
        self.token = attr['refresh']
        return attr
    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail('error_token')

