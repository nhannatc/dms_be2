
import datetime
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from django_filters import filters, FilterSet
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import status, generics
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateAPIView

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer





from .serializer import UserSerializerDelete, UserByAdminSerializer, UserLoginSerializer, ListUserSerializer, \
    UserSerializer, UserPasswordSerializer, UserLogoutSerializer

from .models import UserProfile


# Create your views here.

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['displayName'] = user.displayName
        token['email'] = user.email
        token['role'] = user.role.id
        token['deletedDate'] = user.deletedDate
        # Add more custom fields from your custom user model, If you have a
        # custom user model.
        # ...

        return token

class UserLoginView(APIView):
    permission_classes = ()
    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(
                request,
                username=serializer.validated_data['email'],
                password=serializer.validated_data['password'],
            )
            if user and user.deletedDate is None:
                 refresh = MyTokenObtainPairSerializer.get_token(user)
                 data = {
                    'refresh_token': str(refresh),
                    'access_token': str(refresh.access_token),
                  }
                 return Response(data, status=status.HTTP_200_OK)

            return Response({
                'error_message': 'Email or password is incorrect or email detected!',
                'error_code': 400
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            'error_messages': serializer.errors,
            'error_code': 400
        }, status=status.HTTP_400_BAD_REQUEST)

class LogoutView(APIView):
    serializer_class = UserLogoutSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializers = self.serializer_class(data=request.data)
        serializers.is_valid(raise_exception=True)
        serializers.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListCreateUserView(ListCreateAPIView):
    model = UserProfile
    serializer_class = ListUserSerializer
    permission_classes = (IsAuthenticated,)


    def get_queryset(self):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        if role == 1 and deletedDate == None:# 1 là admin
            return UserProfile.objects.all().filter(deletedDate__isnull=True).order_by('-id')

    def post(self, request, *args, **kwargs):
        role = self.request.user.role_id
        deletedDate = self.request.user.deletedDate
        serializer = ListUserSerializer(data=request.data)
        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            serializer.save()
            return JsonResponse({
                     'message': 'Register successful!'
                    }, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse({
                    'error_message': 'This email has already exist or you not a adminnnn!',
                     'errors_code': 400,
                 }, status=status.HTTP_400_BAD_REQUEST)

class ListDeleteUserView(ListAPIView):
    model = UserProfile
    serializer_class = ListUserSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        role = self.request.user.role_id
        deleted = self.request.user.deletedDate
        if role == 1 and deleted == None:# 1 là admin
            return UserProfile.objects.all().filter(deletedDate__isnull=False).order_by('-id')

class UpdateUserView(RetrieveUpdateAPIView):
    model = UserProfile
    serializer_class = UserSerializer
    queryset = UserProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def get_object(self):
        user = self.request.user
        return user

    def put(self, request, *args, **kwargs):
        user = self.request.user
        deletedDate = self.request.user.deletedDate
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid() and deletedDate == None:
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                    'message': 'Update User successful!'
                }, status=status.HTTP_200_OK)

        return JsonResponse({
                'message': 'Update User unsuccessful!'
            }, status=status.HTTP_400_BAD_REQUEST)

class DeleteUserView(RetrieveUpdateAPIView):
    model = UserProfile
    serializer_class = UserSerializerDelete
    queryset = UserProfile.objects.all()

    permission_classes = (IsAuthenticated,)
    lookup_field = "pk"

    def get_object(self):
        user = self.request.user
        return user

    def put(self, request, *args, **kwargs):
        user = self.request.user
        deletedDate = self.request.user.deletedDate
        serializer = UserSerializerDelete(user, data=request.data)
        if serializer.is_valid() and deletedDate == None:
            serializer.validated_data['deletedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                'message': 'Update User successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Delete User unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)
class UpdateUserFormAdminView(RetrieveUpdateAPIView):
    model = UserProfile
    serializer_class = UserByAdminSerializer
    queryset = UserProfile.objects.all()
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        user = get_object_or_404(UserProfile, id=kwargs.get('pk'))
        serializer = UserByAdminSerializer(user, data=request.data)
        deletedDate = self.request.user.deletedDate
        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                    'message': 'Update User successful!'
                }, status=status.HTTP_200_OK)

        return JsonResponse({
                'message': 'Update User unsuccessful!'
            }, status=status.HTTP_400_BAD_REQUEST)

class DeleteUserFormAdminView(RetrieveUpdateAPIView):
    model = UserProfile
    serializer_class = UserSerializerDelete
    queryset = UserProfile.objects.all()

    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        role = self.request.user.role_id
        user = get_object_or_404(UserProfile, id=kwargs.get('pk'))
        deletedDate = self.request.user.deletedDate
        serializer = UserSerializerDelete(user, data=request.data)
        if serializer.is_valid() and role == 1 and deletedDate == None:
            serializer.validated_data['deletedDate'] = datetime.datetime.now()
            serializer.save()
            return JsonResponse({
                'message': 'Update User successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Delete User unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)


class UpdatePasswordUserView(RetrieveUpdateAPIView):
    model = UserProfile
    serializer_class = UserPasswordSerializer
    permission_classes = (IsAuthenticated,)
    queryset = UserProfile.objects.all()

    def get_object(self):
        return self.request.user

    def put(self, request, *args, **kwargs):
        user = self.request.user
        old_password = request.data['old_password']
        deletedDate = self.request.user.deletedDate
        serializer = UserPasswordSerializer(user, data=request.data)
        if serializer.is_valid() and deletedDate == None:
            if not user.check_password(old_password):
                return JsonResponse({'old_password': ['Wrong password.']
                                     }, status=status.HTTP_400_BAD_REQUEST)
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            serializer.validated_data['updatedDate'] = datetime.datetime.now()
            serializer.save()

            return JsonResponse({
                'message': 'Update User password successful!'
            }, status=status.HTTP_200_OK)

        return JsonResponse({
            'message': 'Update User password unsuccessful!'
        }, status=status.HTTP_400_BAD_REQUEST)

class UserFilter(FilterSet):
     name = filters.CharFilter(field_name="displayName", lookup_expr='contains')
     email = filters.CharFilter(field_name="email", lookup_expr='contains')
     role = filters.CharFilter(field_name="role__name", lookup_expr='exact')
     class Meta:
         model = UserProfile
         fields = ('email', 'displayName', 'role', 'role__name')

class ListSearchallView(generics.ListAPIView):
     model = UserProfile
     serializer_class = ListUserSerializer
     permission_classes = (IsAuthenticated,)
     queryset = UserProfile.objects.all().filter(deletedDate__isnull=True).order_by('-id')
     filter_backends = [DjangoFilterBackend]
     filterset_class = UserFilter